# Configures the window (platform-agnostic)

from tkinter import *
from tkinter import ttk
from tkinter.ttk import Style

root = Tk()
root.title("TLetris")

mainframe = ttk.Frame(root, padding="3 3 12 12")
mainframe.grid(column=0, row=0, sticky=(N, W, E, S))
root.columnconfigure(0, weight=1)
root.rowconfigure(0, weight=1)

logo = Label(mainframe, text='TLetris', )
logo.grid(column=2, row=1, sticky=(W, E))

root.columnconfigure(0, weight=1)
root.rowconfigure(0, weight=1)
mainframe.columnconfigure(2, weight=3)

root.mainloop()
